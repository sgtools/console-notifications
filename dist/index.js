"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppConsoleNotifications = exports.NotificationType = void 0;
const console_1 = require("@sgtools/console");
var NotificationType;
(function (NotificationType) {
    NotificationType["Success"] = "success";
    NotificationType["Warn"] = "warn";
    NotificationType["Error"] = "error";
})(NotificationType = exports.NotificationType || (exports.NotificationType = {}));
class AppConsoleNotifications {
    constructor() {
        this.notifications = [];
    }
    clear() {
        this.notifications = [];
    }
    add(type, message) {
        this.notifications.push({ type: type, message: message });
    }
    addSuccess(message) {
        this.add(NotificationType.Success, message);
    }
    addWarning(message) {
        this.add(NotificationType.Warn, message);
    }
    addError(message) {
        this.add(NotificationType.Error, message);
    }
    show() {
        this.notifications.filter(n => n.type === NotificationType.Success).forEach(n => {
            console_1.console.success(n.message);
        });
        this.notifications.filter(n => n.type === NotificationType.Warn).forEach(n => {
            console_1.console.warn(n.message);
        });
        this.notifications.filter(n => n.type === NotificationType.Error).forEach(n => {
            console_1.console.error(n.message);
        });
        if (this.notifications.length > 0)
            console_1.console.newline();
        this.clear();
    }
}
exports.AppConsoleNotifications = AppConsoleNotifications;
console_1.AppConsole.prototype.notifications = new AppConsoleNotifications();
