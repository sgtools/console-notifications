export declare enum NotificationType {
    Success = "success",
    Warn = "warn",
    Error = "error"
}
export declare class AppConsoleNotifications {
    notifications: {
        type: NotificationType;
        message: string;
    }[];
    constructor();
    private clear;
    private add;
    addSuccess(message: string): void;
    addWarning(message: string): void;
    addError(message: string): void;
    show(): void;
}
declare module '@sgtools/console' {
    interface AppConsole {
        notifications: AppConsoleNotifications;
    }
}
