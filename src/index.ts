import { AppConsole, console } from '@sgtools/console';

export enum NotificationType
{
    Success = 'success',
    Warn    = 'warn',
    Error   = 'error'
}

export class AppConsoleNotifications
{
    notifications: { type: NotificationType, message: string }[];

    constructor()
    {
        this.notifications = [];
    }

    private clear()
    {
        this.notifications = [];
    }

    private add(type: NotificationType, message: string)
    {
        this.notifications.push({ type: type, message: message });
    }

    public addSuccess(message: string)
    {
        this.add(NotificationType.Success, message);
    }

    public addWarning(message: string)
    {
        this.add(NotificationType.Warn, message);
    }

    public addError(message: string)
    {
        this.add(NotificationType.Error, message);
    }

    public show()
    {
        this.notifications.filter(n => n.type === NotificationType.Success).forEach(n => {
            console.success(n.message);
        });
        this.notifications.filter(n => n.type === NotificationType.Warn).forEach(n => {
            console.warn(n.message);
        });
        this.notifications.filter(n => n.type === NotificationType.Error).forEach(n => {
            console.error(n.message);
        });
        if (this.notifications.length > 0) console.newline();
        this.clear();
    }
}

declare module '@sgtools/console'
{
    export interface AppConsole
    {
        notifications: AppConsoleNotifications;
    }
}

AppConsole.prototype.notifications = new AppConsoleNotifications();