[@sgtools/console-notifications](README.md) / Exports

# @sgtools/console-notifications

## Table of contents

### Enumerations

- [NotificationType](enums/notificationtype.md)

### Classes

- [AppConsoleNotifications](classes/appconsolenotifications.md)
