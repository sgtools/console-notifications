[@sgtools/console-notifications](../README.md) / [Exports](../modules.md) / NotificationType

# Enumeration: NotificationType

## Table of contents

### Enumeration members

- [Error](notificationtype.md#error)
- [Success](notificationtype.md#success)
- [Warn](notificationtype.md#warn)

## Enumeration members

### Error

• **Error**: = "error"

Defined in: index.ts:7

___

### Success

• **Success**: = "success"

Defined in: index.ts:5

___

### Warn

• **Warn**: = "warn"

Defined in: index.ts:6
