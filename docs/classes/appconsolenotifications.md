[@sgtools/console-notifications](../README.md) / [Exports](../modules.md) / AppConsoleNotifications

# Class: AppConsoleNotifications

## Hierarchy

* **AppConsoleNotifications**

## Table of contents

### Constructors

- [constructor](appconsolenotifications.md#constructor)

### Properties

- [notifications](appconsolenotifications.md#notifications)

### Methods

- [add](appconsolenotifications.md#add)
- [addError](appconsolenotifications.md#adderror)
- [addSuccess](appconsolenotifications.md#addsuccess)
- [addWarning](appconsolenotifications.md#addwarning)
- [clear](appconsolenotifications.md#clear)
- [show](appconsolenotifications.md#show)

## Constructors

### constructor

\+ **new AppConsoleNotifications**(): [*AppConsoleNotifications*](appconsolenotifications.md)

**Returns:** [*AppConsoleNotifications*](appconsolenotifications.md)

Defined in: index.ts:12

## Properties

### notifications

• **notifications**: { `message`: *string* ; `type`: [*NotificationType*](../enums/notificationtype.md)  }[]

Defined in: index.ts:12

## Methods

### add

▸ `Private`**add**(`type`: [*NotificationType*](../enums/notificationtype.md), `message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`type` | [*NotificationType*](../enums/notificationtype.md) |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:24

___

### addError

▸ **addError**(`message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:39

___

### addSuccess

▸ **addSuccess**(`message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:29

___

### addWarning

▸ **addWarning**(`message`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`message` | *string* |

**Returns:** *void*

Defined in: index.ts:34

___

### clear

▸ `Private`**clear**(): *void*

**Returns:** *void*

Defined in: index.ts:19

___

### show

▸ **show**(): *void*

**Returns:** *void*

Defined in: index.ts:44
